import React from 'react';
import './App.css';
import HomePage from './pages/HomePage/HomePage.component'
import ShopPage from './pages/ShopPage/ShopPage.component'
import {Route, Switch } from 'react-router-dom'
import Header from './components/Header/header.component'
import  SignInAndSignUp from './components/sign-in-and-sign-up/sign-in-and-sign-up.component'
import { auth } from  './firebase/firebase.utils'



class App extends React.Component {

  constructor(props) {
    super(props)
  
    this.state = {

       currentUser: null
    }
  }

  unsubscribeFromAuth = null;

  componentDidMount(){
    this.unsubscribeFromAuth = auth.onAuthStateChanged(user => {
      this.setState({
        currentUser:user
      })
    })
  }
  componentWillUnmount(){
    this.unsubscribeFromAuth()
  }
  

  render(){

    return (
      <div>
  <Header currentUser={this.state.currentUser}/>
        <Switch>
          <Route exact path="/" component={HomePage}/>
          <Route  path="/shop" component={ShopPage}/>
          <Route  path="/signin" component={SignInAndSignUp}/>
  
        </Switch>
  
      </div>
    );
  }

}

export default App;
