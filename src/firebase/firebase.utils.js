import firebase from 'firebase/app' ;
import 'firebase/firestore';     
import 'firebase/auth';

const config = {   apiKey: "AIzaSyDf6yclNg7reD0Wj91mkdxpVnnNRjJa0CI",
    authDomain: "crwn-db-e5980.firebaseapp.com",
    databaseURL: "https://crwn-db-e5980.firebaseio.com",
    projectId: "crwn-db-e5980",
    storageBucket: "crwn-db-e5980.appspot.com",
    messagingSenderId: "750311771184",
    appId: "1:750311771184:web:53d367e9343b2524a2f1f5"
  };
firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();
 const provider = new firebase.auth.GoogleAuthProvider();
 provider.setCustomParameters({prompt: 'select_account'});
 export const signInWithGoogle = () => auth.signInWithPopup(provider)
 
 export default firebase;